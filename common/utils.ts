export const isObject = (args?: any) => {
  return args && (JSON.parse(JSON.stringify(args))).constructor === Object;
} 