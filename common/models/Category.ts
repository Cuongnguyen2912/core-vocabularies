import Course from "./Course";

export default class Category {
  _id: any;
  parentId: string | null;
  name: string;
  type: number;
  status: number;

  courses?: Course[];

  constructor(args: any = {}) {
    this._id = args._id;
    this.parentId = args._id;
    this.name = args.name;
    this.type = args.type;
    this.status = args.status;
  }
}