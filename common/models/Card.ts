export default class Card {
  _id: any;
  parentId: string;
  question: {
    text: string;
    image: string;
    sound: string;
    hint: string;
  };
  answer: {
    texts: string[];
    choices: string[];
    image: string;
    hint: string;
  };
  hasChild: number;
  type: number;
  status: number;

  constructor(args: any = {}) {
    this._id = args._id;
    this.parentId = args.parentId;
    this.question = {
      text: args.question.text,
      image: args.question.image,
      sound: args.question.sound,
      hint: args.question.hint
    };
    this.answer = {
      texts: args.answer.texts,
      choices: args.answer.choices,
      image: args.answer.image,
      hint: args.answer.hint
    };
    this.hasChild = args.hasChild;
    this.type = args.type;
    this.status = args.status;
  }
}