export default class Course {
  _id: any;
  name: string;
  status: number;

  constructor(args: any = {}) {
    this._id = args._id;
    this.name = args.name;
    this.status = args.status;
  }
}