const fs = require("fs");
const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const DotenvWebpackPlugin = require("dotenv-webpack");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const CompressionWebpackPlugin = require("compression-webpack-plugin");
const dotenv = require('dotenv');
const dotenvExpand = require('dotenv-expand');

const directoryPath = path.resolve("public");

const handleDir = () => {
  return new Promise((resolve, reject) => {
    fs.readdir(directoryPath, (err, files) => {
      if (err) {
        reject("Unable to scan directory: " + err)
      }
      resolve(files)
    })
  })
}

module.exports = async (env, argv) => {
  const isDev = argv.mode === "development";
  const dotenvFiles = [
    `.env`, `.env.${argv.mode}`
  ].filter(Boolean);

  const dotenvConfig = () => {
    dotenvFiles.forEach((path) => {
      if (fs.existsSync(path)) {
        dotenvExpand(dotenv.config({ path }))
      }
    })
  };

  dotenvConfig();

  const dirs = await handleDir();
  const copyPluginPatterns = dirs
    .filter(dir => dir !== "index.html")
    .map(dir => {
      return {
        from: dir,
        context: path.resolve("public")
      }
    })
  const basePlugins = [
    new HtmlWebpackPlugin({ template: "public/index.html" }),
    new CopyWebpackPlugin({ patterns: copyPluginPatterns }),
    new MiniCssExtractPlugin({ filename: isDev ? "site.css" : "static/css/site.min.css" }),
    new webpack.ProgressPlugin()
  ]

  const devPlugins = [
    ...basePlugins,
    new DotenvWebpackPlugin({ path: `./.env.development` }),
  ]

  const prodPlugins = [
    ...basePlugins,
    new DotenvWebpackPlugin({ path: `./.env.production` }),
    new CleanWebpackPlugin(),
    new CompressionWebpackPlugin({
      test: /\.(css|js|html|svg)$/
    })
  ]

  return {
    entry: [
      "./src/index.tsx"
    ],
    output: {
      filename: "index.bundle.js",
      path: path.resolve(__dirname, "build"),
      library: "CoreVocabularies",
      publicPath: process.env.PUBLIC_URL || '/'
    },
    module: {
      rules: [
        {
          test: /\.(ts|tsx|js|jsx)$/,
          use: ["ts-loader"],
          exclude: /node_modules/
        },
        {
          test: /\.(s[ac]ss|css)$/,
          use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
        },
        {
          test: /\.(eot|ttf|woff|woff2)$/,
          use: [
            {
              loader: "file-loader",
              options: {
                name: isDev ? "[path][name].[ext]" : "[name].[ext]"
              }
            }
          ]
        },
        {
          test: /\.(png|svg|jpg|gif)$/,
          use: [
            {
              loader: "file-loader",
              options: {
                name: isDev
                  ? "[path][name].[ext]"
                  : "[name].[contenthash:6].[ext]"
              }
            }
          ]
        }
      ]
    },
    resolve: {
      extensions: [".tsx", ".ts", ".jsx", ".js"],
      alias: {
        "@": path.resolve("src"),
        "@@": path.resolve()
      },
      fallback: { crypto: false },
    },
    devtool: isDev ? "source-map" : false,
    devServer: {
      port: 3000,
      hot: true,
      historyApiFallback: true,
      open: true
    },
    plugins: isDev ? devPlugins : prodPlugins,
    performance: {
      maxEntrypointSize: 800000
    },
  }
}
