import Card from "@@/../common/models/Card";
import Topic from "@@/../common/models/Topic";
import { Box, Button, CircularProgress, Container, Grid, MenuItem, Paper, Select, Theme, Tooltip, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { useSnackbar } from "notistack";
import React, { Fragment, useCallback, useEffect, useReducer, useState } from "react";
import ReactAudioPlayer from "react-audio-player";
import { v4 } from "uuid";
import { apiGetCategories } from "./apis/course.apis";
import { apiGetTopicCardsAndVocabs, apiGetTopics } from "./apis/topic.apis";
import { apiCreateOneVocab, apiCreateVocabs, apiSyncCardsSound } from "./apis/vocab.apis";
import { appInitState, appReducer, setCategoriesAction, setCurrentChildCategoryIdAction, setCurrentCourseIdAction, setCurrentRootCategoryIdAction, setMapSyncingAction } from "./app.logic";
import TopicTreeItem from "./components/TopicTreeItem";
import UploadButton from "./components/UploadButton";
import { useAppDispatch, useAppSelector } from "./hooks";
import useSocket from "./hooks/useSocket";
import { setListTopics, setTopicCards, setTopicLoading, updateCardsAfterCreate, updateCardSyncedSoundUrl } from "./redux/topic.slice";

const socketURL = process.env.REACT_APP_SOCKET_URL;

const useStyles = makeStyles((theme: Theme) => ({
  boxContent: {
    padding: "16px"
  }
}));

const App = () => {
  const [state, uiLogic] = useReducer(appReducer, appInitState);
  const classes = useStyles();
  const dispatch = useAppDispatch();
  const mapTopic = useAppSelector((state) => state.topicReducer.mapTopic);
  const currentTopic = useAppSelector((state) => state.topicReducer.currentTopic);
  const topicLoading = useAppSelector((state) => state.topicReducer.topicLoading);
  const cards = useAppSelector((state) => state.topicReducer.cards);
  const { enqueueSnackbar } = useSnackbar();
  const [clientId, setClientId] = useState('');
  const { socket, leave } = useSocket({ enabled: !!socketURL && !!clientId, url: `${socketURL}/vocabs`, roomId: clientId });

  useEffect(() => {
    apiGetCategories()
      .then((data) => {
        uiLogic(setCategoriesAction(data, true));
      })
    if (currentTopic) {
      apiGetTopicCardsAndVocabs({ topicId: currentTopic?.topicExercise?._id })
        .then((cards) => {
          dispatch(setTopicCards(cards));
        })
        .finally(() => {
          dispatch(setTopicLoading(false));
        });
    } else {
      dispatch(setTopicLoading(false));
    }

    let clientId = window.localStorage.getItem("_vocab_sync_client") ?? '';
    if (!clientId) {
      clientId = v4();
      window.localStorage.setItem("_vocab_sync_client", clientId);
    }
    setClientId(clientId);
  }, []);

  useEffect(() => {
    if (socket) {
      socket.on("vocabs-topic-done", (data: { topic: Topic; cards: Array<Card & { syncUrl?: string }> }) => {
        dispatch(updateCardsAfterCreate({ cards: data?.cards }));
        uiLogic(setMapSyncingAction(data?.topic?._id, false));
        enqueueSnackbar(`Đã tải/đồng bộ bài học: ${data?.topic?.name}`, { variant: "info" });
      });
    }
    return () => {
      if (socket) {
        leave();
      }
    }
  }, [socket]);


  const handleChangeRootCategory = (_id: string) => {
    apiGetCategories({ parentId: _id })
      .then((data) => {
        uiLogic(setCategoriesAction(data));
      })
    uiLogic(setCurrentRootCategoryIdAction(_id));
  }

  const handleChangeChildCategory = (_id: string) => {
    uiLogic(setCurrentChildCategoryIdAction(_id));
  }

  const handleChangeCourse = (_id: string) => {
    apiGetTopics({ courseId: _id })
      .then((topics) => {
        // uiLogic(setListTopicsAction('main', topics));
        dispatch(setListTopics({ topics, parentTopicId: 'main' }));
      })
    uiLogic(setCurrentCourseIdAction(_id));
  }

  const handleCreateOneVocab = (args: { text: string; cardId: string; index: number }) => {
    const { text, cardId, index } = args;
    apiCreateOneVocab({ text, cardId })
      .then(({ soundUrl: url }) => {
        enqueueSnackbar("Đã tải và đồng bộ", { variant: "success" });
        dispatch(updateCardSyncedSoundUrl({ index, url }));
      });
  }

  const handleCreateVocabs = (args: { topicId: string, sync?: boolean, forceReplace?: boolean }) => {
    const { topicId, sync, forceReplace } = args;
    if (!!topicId && clientId) {
      if (!!state.mapSyncing[topicId]) {
        enqueueSnackbar(`Đang ${sync ? 'tải và đồng bộ' : 'tải'}`, { variant: "warning" });
        return;
      }
      uiLogic(setMapSyncingAction(topicId, true));
      apiCreateVocabs({ topicId, clientId, sync, forceReplace })
        .then(({ error }) => {
          if (error) {
            enqueueSnackbar("Có lỗi xảy ra", { variant: "error" });
          } else {
            enqueueSnackbar(`Đã yêu cầu tải${sync ? ' và đồng bộ' : ''}`, { variant: "info" });
          }
        })
    }
  }

  const handleSyncCards = () => {
    if (cards.length) {
      const cardIds = cards.filter((card) => card.question.sound !== card.syncUrl).map((e) => e._id);
      apiSyncCardsSound({ cardIds })
        .then((cards) => {
          dispatch(updateCardsAfterCreate({ cards }));
          enqueueSnackbar("Đã đồng bộ", { variant: "success" });
        })
    }
  }

  return (<Container maxWidth="xxl">
    <Paper elevation={1} className={classes.boxContent}>
      {/* CATEGORIES */}
      <Grid container spacing={1}>
        {/* Root Categories */}
        <Grid item xs={12} sm={6}>
          <Typography component="div">Danh mục cha:</Typography>
          <Select
            value={state.currentRootCategoryId}
            onChange={(e) => handleChangeRootCategory(e.target.value)}
            size="small"
            fullWidth
          >
            <MenuItem value={'--root--'} disabled><em>-- Chọn danh mục cha --</em></MenuItem>
            {state.rootCategories.map((e) => (
              <MenuItem key={e._id} value={e._id}>{e.name}</MenuItem>
            ))}
          </Select>
        </Grid>

        {/* Child Categories */}
        <Grid item xs={12} sm={6}>
          <Typography component="div">Danh mục con:</Typography>
          <Select
            value={state.currentChildCategoryId}
            onChange={(e) => handleChangeChildCategory(e.target.value)}
            size="small"
            fullWidth
            disabled={!state.childCategories.length}
          >
            <MenuItem value={'--root--'} disabled><em>-- Chọn danh mục con --</em></MenuItem>
            {state.childCategories.map((e) => (
              <MenuItem key={e._id} value={e._id}>{e.name}</MenuItem>
            ))}
          </Select>
        </Grid>
      </Grid>

      {/* COURSES */}
      <Typography component="div">Khoá học:</Typography>
      <Select
        value={state.currentCourseId}
        onChange={(e) => handleChangeCourse(e.target.value)}
        size="small"
        fullWidth
        disabled={!state.currentCategory?.courses?.length}
      >
        <MenuItem value={'--root--'} disabled><em>-- Chọn khoá học --</em></MenuItem>
        {(state.currentCategory?.courses ?? []).map((e) => (
          <MenuItem key={e._id} value={e._id}>{e.name}</MenuItem>
        ))}
      </Select>
    </Paper>

    <Paper elevation={1} className={classes.boxContent} sx={{ minHeight: "576px" }}>
      <Grid container spacing={1}>
        {/* Tree */}
        <Grid item xs={12} sm={4}>
          <Box sx={{
            maxHeight: "576px",
            overflowY: "auto"
          }}>
            <Typography component="h2">Danh sách bài học:</Typography>
            {mapTopic['main'].map((mainTopic) => (
              <Fragment key={mainTopic._id}>
                <Box mt="8px">
                  <TopicTreeItem topic={mainTopic} />
                </Box>
              </Fragment>
            ))}
          </Box>
        </Grid>

        {/* Cards Info */}
        <Grid item xs={12} sm={8}>
          <Paper elevation={1} className={classes.boxContent} sx={{ height: "560px" }}>
            {topicLoading
              ? <><CircularProgress /></>
              : (<>
                {!!currentTopic && currentTopic.topicExercise?.contentType === 6
                  ? <>
                    <Box display="flex">
                      <Box flex={1}>
                        <Typography>Tên bài học: <b>{currentTopic.name}</b></Typography>
                        <ul>
                          <li>Tổng số: <b>{cards.length}</b> từ</li>
                          <li>Đã tải về thư viện: <b>{cards.filter(({ syncUrl }) => !!syncUrl).length}</b> từ</li>
                          <li>Đã đồng bộ: <b>{cards.filter(({ question: { sound }, syncUrl }) => !!syncUrl && sound === syncUrl).length}</b> từ</li>
                        </ul>
                      </Box>

                      <Box display="flex" flexDirection="column" justifyContent="center" gap="8px" mb="8px" alignItems="center">
                        <Tooltip title="Tải tất cả từ về thư viện (tải lại), đồng thời gắn vào card" arrow placement="top">
                          <Button
                            variant="contained" color="secondary" fullWidth onClick={() => handleCreateVocabs({ topicId: currentTopic._id, sync: true, forceReplace: true })}>
                            Tải và đồng bộ tất cả
                          </Button>
                        </Tooltip>

                        <Tooltip title="Tải tất cả từ về thư viện (tải lại), không gắn vào card" arrow placement="top">
                          <Button
                            variant="contained" color="success" fullWidth onClick={() => handleCreateVocabs({ topicId: currentTopic._id, sync: false, forceReplace: true })}>
                            Tải tất cả
                          </Button>
                        </Tooltip>

                        <Tooltip title="Gắn vào card với những từ đã được tải về từ thư viện" arrow placement="top">
                          <Button variant="contained" fullWidth onClick={handleSyncCards}>
                            Đồng bộ tất cả
                          </Button>
                        </Tooltip>
                      </Box>
                    </Box>

                    <Box sx={{ overflowY: "auto", height: "400px" }}>
                      {cards.map((card, index) => {
                        return (
                          <Paper key={card._id} elevation={2} sx={{ marginTop: "4px" }}>
                            <Grid container>
                              <Grid item xs={8}>
                                <Box display="flex" alignItems="center" justifyContent="space-between">
                                  <Box>{card.question.text}</Box>
                                  <Box>
                                    <ReactAudioPlayer controls={true} src={card.syncUrl} />
                                  </Box>
                                </Box>
                              </Grid>

                              <Grid item xs={4}>
                                <Box display="flex" alignItems="center" justifyContent="space-around" height="100%">
                                  <Tooltip title={`Tải âm thanh "${card.question.text}" và gắn vào card`} arrow placement="top">
                                    <Button
                                      variant="contained" size="small" color="secondary"
                                      onClick={() => {
                                        handleCreateOneVocab({ text: card.question.text, cardId: card._id, index })
                                      }}
                                    >
                                      {card.syncUrl && card.question.sound === card.syncUrl ? 'Tải và đồng bộ lại' : 'Tải và đồng bộ'}
                                    </Button>
                                  </Tooltip>
                                  <UploadButton
                                    label={card.syncUrl && card.question.sound === card.syncUrl ? 'Tải lại' : 'Tải file'}
                                    text={card.question.text}
                                    cardId={card._id}
                                    key={Math.random()}
                                    onUploadFinished={(soundUrl) => {
                                      dispatch(updateCardSyncedSoundUrl({ index, url: soundUrl }));
                                    }}
                                    onUploadError={() => {
                                      enqueueSnackbar("Tải lên thất bại", { variant: "error" });
                                    }}
                                    onUploadSuccess={() => {
                                      enqueueSnackbar("Đã tải và đồng bộ từ file", { variant: "success" });
                                    }}
                                  />
                                </Box>
                              </Grid>
                            </Grid>
                          </Paper>
                        )
                      })}
                    </Box>
                  </>
                  : <></>}
              </>)
            }
          </Paper>
        </Grid>
      </Grid>
    </Paper>
  </Container>);
}

export default App;
