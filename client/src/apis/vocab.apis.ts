import { POST_FILE } from './index';
import Card from "@@/../common/models/Card";
import { POST } from ".";

export const apiCreateOneVocab = async (args: { text: string; cardId: string }): Promise<{ soundUrl: string; card?: Card | null }> => {
  const { data, status } = await POST('vocabs/create-one', args);
  if (status !== 200) return { soundUrl: '' };
  return data;
}

export const apiCreateVocabs = async (args: { topicId: string; clientId: string; sync?: boolean; forceReplace?: boolean }): Promise<{ error: boolean; message?: string }> => {
  const { data, status } = await POST('vocabs', args);
  if (status !== 200) return { error: true };
  return { error: false, ...data }
}

export const apiSyncCardsSound = async (args: { cardIds: string[] }): Promise<Array<Card & { syncUrl?: string }>> => {
  const { data, status } = await POST('sync-cards-sound', args);
  if (status !== 200) return [];
  return data;
}

export const apiUploadFileVocab = async (args: { text: string, file: File, cardId: string }) => {
  const { text, file, cardId } = args;
  const { data, status } = await POST_FILE(`vocabs/upload-file?text=${text}&cardId=${cardId}`, file);
  if (status !== 200) return '';
  return data;
}