import axios from "axios";

const request = axios.create({
  baseURL: `${process.env.REACT_APP_ENDPOINT || 'http://localhost:8081'}/api-v/`,
  timeout: 30000
});

export const GET = async (url: string) => {
  try {
    const { data, status } = await request.get(url);
    return { data, status };
  } catch (error) {
    if (axios.isAxiosError(error)) {
      return {
        data: error.response?.data,
        status: error.response?.status ?? 500
      }
    }
    return { status: 500 };
  }
}

export const POST = async (url: string, reqBody?: any) => {
  try {
    const { data, status } = await request.post(url, reqBody);
    return { data, status };
  } catch (error) {
    if (axios.isAxiosError(error)) {
      return {
        data: error.response?.data,
        status: error.response?.status ?? 500
      }
    }
    return { status: 500 };
  }
}

export const POST_FILE = async (url: string, file: File, reqBody?: any) => {
  try {
    const formData = new FormData();
    formData.append('file', file);
    const { data, status } = await request.post(url, formData, {
      headers: {  
        "Content-Type": "multipart/form-data"
      }
    })
    return { data, status };
  } catch (error) {
    if (axios.isAxiosError(error)) {
      return {
        data: error.response?.data,
        status: error.response?.status ?? 500
      }
    }
    return { status: 500 };
  }
} 