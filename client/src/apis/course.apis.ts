import Category from "@@/../common/models/Category";
import { GET } from ".";

export const apiGetCategories = async (args: { parentId?: string ; limitCourses?: number } = {}): Promise<Category[]> => {
  const { parentId, limitCourses } = args;
  const requestParams = {};
  if (typeof parentId !== "undefined") Object.assign(requestParams, { parentId });
  if (typeof limitCourses !== "undefined") Object.assign(requestParams, { limitCourses });
  const { data, status } = await GET(`categories?${new URLSearchParams(requestParams).toString()}`);
  if (status !== 200) return [];
  return data;
}