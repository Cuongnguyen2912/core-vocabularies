import Card from "@@/../common/models/Card";
import Topic from "@@/../common/models/Topic";
import { GET } from ".";

export const apiGetTopics = async (args: { parentId?: string; courseId: string; }): Promise<Topic[]> => {
  const { parentId, courseId } = args;
  const requestParams = { courseId };
  if (typeof parentId !== "undefined") Object.assign(requestParams, { parentId });
  const { data, status } = await GET(`topics?${new URLSearchParams(requestParams).toString()}`);
  if (status !== 200) return [];
  return data;
}

export const apiGetTopicCardsAndVocabs = async (args: { topicId: string }): Promise<Array<Card & { syncUrl?: string }>> => {
  const { topicId } = args;
  const { data, status } = await GET(`topics/${topicId}/cards-vocabs`);
  if (status !== 200) return [];
  return data;
}