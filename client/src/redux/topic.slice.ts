import Card from "@@/../common/models/Card";
import Topic from "@@/../common/models/Topic";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

type TopicState = {
  mapTopic: {
    [parentTopicId: string]: Topic[];
  },
  currentTopic: Topic | null;
  cards: Array<Card & { syncUrl?: string }>;
  topicLoading: boolean;
}

const initTopicState: TopicState = {
  mapTopic: {
    ['main']: []
  },
  currentTopic: null,
  cards: [],
  topicLoading: false
}

export const topicSlice = createSlice({
  name: 'topic',
  initialState: initTopicState,
  reducers: {
    setListTopics: (state, action: PayloadAction<{ topics: Topic[]; parentTopicId: string }>) => {
      state.mapTopic[action.payload.parentTopicId] = action.payload.topics;
    },
    setCurrentTopic: (state, action: PayloadAction<Topic | null>) => {
      state.currentTopic = action.payload
    },
    setTopicCards: (state, action: PayloadAction<Array<Card & { syncUrl?: string }>>) => {
      state.cards = action.payload;
    },
    setTopicLoading: (state, action: PayloadAction<boolean>) => {
      state.topicLoading = action.payload
    },
    updateCardSyncedSoundUrl: (state, action: PayloadAction<{ index: number; url: string }>) => {
      const { index, url } = action.payload; {
        console.log(index, url);
        if (index !== -1 && !!url) {
          const _cards = [...state.cards];
          const _card = _cards[index];
          _cards.splice(index, 1, {
            ..._card,
            question: {
              ..._card.question,
              sound: url
            },
            syncUrl: url
          });
          state.cards = _cards;
        }
      }
    },
    updateCardsAfterCreate: (state, action: PayloadAction<{ cards: Array<Card & { syncUrl?: string }> }>) => {
      const _cards = [...state.cards];
      _cards.map((card) => {
        const cardNew = action.payload.cards.find(({ _id }) => String(_id) === String(card._id));
        if (cardNew) {
          card.syncUrl = cardNew.syncUrl;
          card.question.sound = cardNew.question.sound;
        }
      });
      state.cards = _cards;
    }
  }
});

export const { setListTopics, setCurrentTopic, setTopicCards, setTopicLoading, updateCardSyncedSoundUrl, updateCardsAfterCreate } = topicSlice.actions;
export default topicSlice.reducer;