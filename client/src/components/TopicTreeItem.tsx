import Topic from "@@/../common/models/Topic";
import { Box, Paper, Theme, Typography } from "@mui/material";
import { Fragment, PropsWithoutRef, useMemo, useState } from "react";
// @ts-ignore
import LessonIcon from "../images/lesson-icon.svg";
// @ts-ignore
import ExerciseIcon from "../images/exercise-icon.svg";
// @ts-ignore
import TestIcon from "../images/test-icon.svg";
import { makeStyles } from "@mui/styles";
import { ExpandLess, ExpandMore } from "@mui/icons-material";
import { useAppDispatch, useAppSelector } from "@/hooks";
import { apiGetTopicCardsAndVocabs, apiGetTopics } from "@/apis/topic.apis";
import { setCurrentTopic, setListTopics, setTopicCards, setTopicLoading } from "@/redux/topic.slice";
import classNames from "classnames";

const useStyles = makeStyles((theme: Theme) => ({
  topicTreeItem: {
    display: "flex",
    padding: "8px",
    alignItems: "center",
    cursor: "pointer",
    "&:hover": {
      backgroundColor: "rgba(0, 0, 0, 0.04)"
    }
  },
  itemActive: {
    backgroundColor: "rgba(25, 118, 210, 0.08) !important"
  },
  itemLeftView: {
    flex: 1,
  },
  itemExpand: {
    cursor: "pointer"
  },
  icon: {
    marginRight: "8px"
  }
}));

const TopicTreeItem = (props: PropsWithoutRef<{ topic: Topic; }>) => {
  const { topic } = props;
  const mapTopic = useAppSelector((state) => state.topicReducer.mapTopic);
  const currentTopic = useAppSelector((state) => state.topicReducer.currentTopic);
  const dispatch = useAppDispatch();

  const [expand, setExpand] = useState(false);
  const classes = useStyles();

  const icon = useMemo(() => {
    if (topic.type === 1) return LessonIcon;
    else if (topic.type === 2) return ExerciseIcon;
    else if (topic.type === 3) return TestIcon;
    return '';
  }, [topic.type]);

  const hasChild = useMemo(() => topic.childType !== 0, [topic.childType]);

  const getChildTopics = () => {
    apiGetTopics({ parentId: topic._id, courseId: topic.courseId })
      .then((topics) => {
        dispatch(setListTopics({ topics, parentTopicId: topic._id }))
      })
  }

  const handleClickTopic = () => {
    if (currentTopic?._id !== topic._id) {
      dispatch(setTopicLoading(true));
      if (topic.topicExercise?.contentType === 6) {
        apiGetTopicCardsAndVocabs({ topicId: topic._id })
          .then((cards) => {
            dispatch(setTopicCards(cards));
            dispatch(setCurrentTopic(topic));
            dispatch(setTopicLoading(false));
          })
      } else {
        dispatch(setCurrentTopic(null));
        dispatch(setTopicLoading(false));
      }
    }
  }

  return (
    <>
      <Paper className={classNames(classes.topicTreeItem, currentTopic?._id === topic._id ? classes.itemActive : '')}>
        <Box className={classes.itemLeftView} component="div" onClick={handleClickTopic}>
          <img src={icon} alt={`${topic.type}`} className={classes.icon} width={"16px"} />
          <Typography component="span">{topic.name}</Typography>
        </Box>
        {hasChild && <Box component="div" className={classes.itemExpand} onClick={() => {
          if (!expand) {
            if (!mapTopic[topic._id]) {
              getChildTopics();
            }
          }
          setExpand(!expand);
        }}>
          {expand ? <ExpandLess /> : <ExpandMore />}
        </Box>}
      </Paper>
      {expand && !!mapTopic[topic._id]?.length && <Box ml={"32px"}>
        {mapTopic[topic._id].map((childTopic) => (
          <Fragment key={childTopic._id}>
            <Box mt="8px">
              <TopicTreeItem topic={childTopic} />
            </Box>
          </Fragment>
        ))}
      </Box>}
    </>
  )
}

export default TopicTreeItem;
