import { Schema, Types } from "mongoose";
import Category from "../../../common/models/Category";
import { instance } from "../utils/mongoose";

const categorySchema = new Schema({
  parentId: {
    type: Types.ObjectId,
    ref: 'Category'
  },
  name: String,
  type: Number,
  status: Number
}, { versionKey: false });

export const CategoryModel = instance.model<Category>('Category', categorySchema);