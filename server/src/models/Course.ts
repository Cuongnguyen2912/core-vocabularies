import { Schema } from "mongoose";
import Course from "../../../common/models/Course";
import { instance } from "../utils/mongoose";

const courseSchema = new Schema({
  name: String,
  status: Number
}, { versionKey: false });

export const CourseModel = instance.model<Course>('Course', courseSchema);