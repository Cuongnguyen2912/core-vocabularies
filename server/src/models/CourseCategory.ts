import { Schema, Types } from "mongoose";
import { instance } from "../utils/mongoose";

const courseCategorySchema = new Schema({
  courseId: {
    type: Types.ObjectId,
    ref: 'Course'
  },
  categoryId: {
    type: Types.ObjectId,
    ref: 'Category'
  },
  rootCategoryId: {
    type: Types.ObjectId,
    ref: 'Category'
  },
  status: Number,
}, { versionKey: false });

export const CourseCategoryModel = instance.model('CourseCategory', courseCategorySchema);