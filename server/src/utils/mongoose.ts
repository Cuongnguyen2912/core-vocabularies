import mongoose from "mongoose";
import dotenv from "./dotenv";

dotenv.config();

const connectDatabase = (main?: boolean) => {
  const DB_HOST = (main ? process.env.MAIN_DB_HOST : process.env.DB_HOST) ?? '127.0.0.1';
  const DB_PORT = (main ? process.env.MAIN_DB_PORT : process.env.DB_PORT) ?? '27017';
  const DB_NAME = (main ? process.env.MAIN_DB_NAME : process.env.DB_NAME) ?? '';
  const DB_USER = (main ? process.env.MAIN_DB_USER : process.env.DB_USER) ?? '';
  const DB_PWD = (main ? process.env.MAIN_DB_PWD : process.env.DB_PWD) ?? '';

  const DB_URL = `mongodb://${DB_HOST}:${DB_PORT}`;

  console.log(`[INFO] MongoDB ${main ? 'MAIN' : 'INSTANCE'}:`, { DB_URL, DB_NAME });

  return mongoose
    .createConnection(DB_URL, {
      dbName: DB_NAME,
      auth: {
        username: DB_USER,
        password: DB_PWD,
      },
      authSource: DB_NAME,
    })
}

const mainConnection = connectDatabase(true);
const instance = connectDatabase();


export {
  mainConnection,
  instance
}
