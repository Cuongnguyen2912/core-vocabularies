import { Server as SocketIOServer, Namespace } from "socket.io";
import { Server as HttpServer } from "http";

var io: SocketIOServer;
var VocabsSocket: Namespace;

export const initSocket = (srv: HttpServer, callback?: (io: SocketIOServer) => void) => {
  console.log('[INFO] Initiating Socket Server');
  io = new SocketIOServer(srv, {
    path: "/api-v/socket-io"
  });
  if (callback) {
    callback(io);
  }

  VocabsSocket = io.of('/vocabs');
  VocabsSocket.on("connection", (socket) => {
    console.log('[DEBUG]: Connected Socket Namespace: Vocabs');
    socket.on("join", (roomId: string) => {
      socket.join(roomId);
      console.log(`[DEBUG] VocabSocket - ${roomId} - Join: ${socket.rooms.size}`);
    });

    socket.on("leave", (roomId: string) => {
      socket.leave(roomId);
      console.log(`[DEBUG] VocabSocket - ${roomId} - Leave: ${socket.rooms.size}`);
    });
  });
}

export { io, VocabsSocket };
