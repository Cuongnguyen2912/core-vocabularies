import Card from "../../../../common/models/Card";
import Topic from "../../../../common/models/Topic";
import { TopicModel } from "../../models/Topic";
import { TopicExerciseModel } from "../../models/TopicExercise";
import CardService from "../cards/card.services";
import VocabService from "../vocabularies/vocab.services";

export default class TopicService {
  static async getTopics(args: { parentId: string | null; courseId: string | null; }) {
    const { parentId, courseId } = args;
    if (!courseId) return [];
    const queryFilter = { courseId, parentId, status: { $ne: -1 } };

    const topics = await TopicModel.find(queryFilter).sort({ orderIndex: 1 }).populate({ path: 'topicExerciseId', model: TopicExerciseModel });
    return topics.map((e) => new Topic(e));
  }

  static async getTopicCardsAndVocabs(args: { topicId: string }) {
    const { topicId } = args;
    if (!topicId) return [];
    const cards = await CardService.getCardsByParent({ parentId: topicId });
    return Promise.all(cards.map(async (_card) => {
      const card: Card & { syncUrl?: string } = _card;
      const vocab = await VocabService.getVocabByText({ text: card.question.text });
      card.syncUrl = vocab?.soundUrl ?? "";
      return card;
    }))
  }

  static async getTopicById(args: { topicId: string }) {
    const topic = await TopicModel.findById(args.topicId);
    return topic ? new Topic(topic) : null;
  }
}