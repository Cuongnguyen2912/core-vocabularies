import { Router } from "express";
import asyncHandler from "../../utils/asyncHandler";
import TopicService from "./topic.services";

const router = Router();

router.get('/topics', asyncHandler(async (req, res) => {
  const parentId = typeof req.query.parentId === "string" ? req.query.parentId : null;
  const courseId = typeof req.query.courseId === "string" ? req.query.courseId : null;

  const data = await TopicService.getTopics({ parentId, courseId });
  return res.json(data);
}));

router.get('/topics/:topicId/cards-vocabs', asyncHandler(async (req, res) => {
  const topicId = req.params.topicId;
  const data = await TopicService.getTopicCardsAndVocabs({ topicId });
  return res.json(data);
}));

export default router;