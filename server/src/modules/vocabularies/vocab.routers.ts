import { Router } from "express";
import asyncHandler from "../../utils/asyncHandler";
import VocabService from "./vocab.services";
import { multerHandler } from '../../apis/middlewares/multer';

const router = Router();

router.post('/vocabs/create-one', asyncHandler(async (req, res) => {
  const { text, cardId } = <{ text: string; cardId?: string }>req.body;
  const data = await VocabService.createOneVocab({ text, cardId });
  return res.json(data);
}));

router.post('/vocabs', asyncHandler(async (req, res) => {
  const reqBody = <{ topicId: string; clientId?: string; sync?: boolean; forceReplace?: boolean }>req.body;
  VocabService.createVocabsByTopic(reqBody);
  return res.json({ message: "OK" });
}));

router.post('/vocabs/upload-file', multerHandler.single("file"), asyncHandler(async (req, res) => {
  const { text, cardId } = <{ text: string, cardId: string }>req.query;
  const file = <Express.Multer.File>req.file;
  const data = await VocabService.uploadAndSync({ text, file, cardId });
  return res.json({ soundUrl: data });
}));

export default router;
