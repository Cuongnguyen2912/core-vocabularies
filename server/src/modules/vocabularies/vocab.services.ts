import { Storage } from "@google-cloud/storage";
import axios from "axios";
import Card from "../../../../common/models/Card";
import Vocabulary from "../../../../common/models/Vocabulary";
import { googleStorageCredentials as credentials } from "../../credentials";
import { VocabularyModel } from "../../models/Vocabulary";
import { VocabsSocket } from "../../utils/sockets";
import CardService from "../cards/card.services";
import TopicService from "../topics/topic.services";
import PQueue from "p-queue";

const BUCKET_NAME = "ielts-fighters.appspot.com/";
const BASE_FORDER = "elearning-react/vocabulary/";
const PROJECT_ID = "ielts-fighters";

const storage = new Storage({ credentials, projectId: PROJECT_ID });
const bucket = storage.bucket(BUCKET_NAME);

export default class VocabService {
  static getGoogleTTSURL(query: string) {
    return `https://translate.google.com.vn/translate_tts?ie=UTF-8&q=${query}&tl=en-us&client=tw-ob`;
  }

  static uploadBufferToGCP(args: { buffer: ArrayBuffer; fileName: string }): Promise<string> {
    const { buffer, fileName } = args;
    return new Promise((resolve, reject) => {
      const remoteFile = bucket.file(`${BASE_FORDER}sounds/${fileName}`);
      const blobStream = remoteFile.createWriteStream({
        metadata: {
          contentType: "audio/mpeg",
          contentDisposition: "attachment",
          metadata: {
            originalBytes: buffer.byteLength
          }
        },
        resumable: false
      });

      blobStream
        .on("finish", async () => {
          await remoteFile.makePublic();
          resolve(`https://storage.googleapis.com/${bucket.name}/${remoteFile.name}`);
        })
        .on("error", () => {
          reject('');
        })
        .end(buffer);
    })
  }

  static async createOneVocab(args: { text: string; cardId?: string; }) {
    const { text, cardId } = args;
    if (!text) return { soundUrl: '' };

    const { data, status } = await axios.get(this.getGoogleTTSURL(encodeURIComponent(text)), { responseType: "arraybuffer" });
    if (status !== 200) return { soundUrl: '' };
    const fileName = `${text}.mp3`;
    const soundUrl = await this.uploadBufferToGCP({ buffer: data, fileName });
    VocabularyModel.findOneAndUpdate({ text }, { $set: { soundUrl } }, { upsert: true }).exec();
    if (cardId) {
      const card = await CardService.updateCardById({ _id: cardId, updates: { "question.sound": soundUrl } });
      return { soundUrl, card };
    }
    return { soundUrl, card: null };
  }

  static async getVocabByText(args: { text: string }) {
    const vocab = await VocabularyModel.findOne({ text: args.text });
    return vocab ? new Vocabulary(vocab) : null;
  }

  static async createVocabsByTopic(args: { topicId: string; clientId?: string; sync?: boolean; forceReplace?: boolean }) {
    const syncedCards: Array<Card & { syncUrl?: string }> = [];
    const queue = new PQueue({ concurrency: parseInt(process.env.DOWNLOAD_VOCABS_CONCURRENCY || '50') });
    queue.on("next", async () => {
      if (queue.pending === 0 && queue.size === 0) {
        if (args.clientId) {
          const topic = await TopicService.getTopicById({ topicId: args.topicId });
          VocabsSocket.in(args.clientId).emit('vocabs-topic-done', { topic, cards: syncedCards });
        }
      }
    });
    const cards = await CardService.getCardsByParent({ parentId: args.topicId });

    cards.map((card) => {
      queue.add(async () => {
        const _syncedCard: Card & { syncUrl?: string } = { ...card };
        const vocab = await VocabularyModel.findOne({ text: card.question.text });
        if (!vocab || vocab?.soundUrl !== _syncedCard.question.sound || args.forceReplace) {
          const vocabSoundUrl = vocab?.soundUrl;
          if (vocabSoundUrl && !!vocabSoundUrl.match(/(.*)_manual\.mp3$/g)) {
            _syncedCard.syncUrl = vocabSoundUrl;
            if (args.sync) {
              _syncedCard.question.sound = vocabSoundUrl;
            }
          } else {
            const { soundUrl, card: newCard } = await this.createOneVocab({ text: _syncedCard.question.text, cardId: args.sync ? _syncedCard._id : undefined });
            _syncedCard.syncUrl = soundUrl;
            if (args.sync && newCard) {
              _syncedCard.question.sound = newCard.question.sound;
            }
          }
        }
        syncedCards.push(_syncedCard);
      })
    });
  }

  static async uploadAndSync(args: { text: string, file: Express.Multer.File, cardId: string }) {
    const { text, file, cardId } = args;
    const fileName = `${text.replace(/\s/g, '-')}_manual.mp3`;
    const { buffer } = file;
    const soundUrl = await this.uploadBufferToGCP({ fileName, buffer });
    VocabularyModel.findOneAndUpdate({ text: text }, { $set: { soundUrl } }, { upsert: true }).exec();
    if (cardId) {
      CardService.updateCardById({ _id: cardId, updates: { "question.sound": soundUrl } });
    }
    return soundUrl;
  }
}