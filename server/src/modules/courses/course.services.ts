import Category from "../../../../common/models/Category";
import Course from "../../../../common/models/Course";
import { CategoryModel } from "../../models/Category";
import { CourseModel } from "../../models/Course";
import { CourseCategoryModel } from "../../models/CourseCategory";

export default class CourseService {
  static async getCategories(args: { parentId: string | null; limitCourses?: number }) {
    const { parentId, limitCourses } = args;
    const categories = await CategoryModel.find({ parentId });
    if (limitCourses !== 0) {
      const data = await Promise.all(categories.map(async (_category) => {
        const category = new Category(_category);
        const courseIds = (
          await CourseCategoryModel.find({ categoryId: category._id }, 'courseId')
        ).map((e) => e.get('courseId'));

        let courseQuery = CourseModel.find({ _id: { $in: courseIds }, status: { $in: [1, 4] } });
        if (typeof limitCourses !== 'undefined') courseQuery = courseQuery.limit(limitCourses);

        const courses = await courseQuery.exec();
        category.courses = courses.map((e) => new Course(e));
        return category;
      }));
      return data;
    }
    return categories;
  }
}