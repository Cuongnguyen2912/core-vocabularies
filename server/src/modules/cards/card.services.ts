import _ from "lodash";
import Card from "../../../../common/models/Card";
import { STATUS_PUBLIC } from "../../../../common/config";
import { CardModel } from "../../models/Card";
import VocabService from "../vocabularies/vocab.services";

export type CardFields = Extract<keyof Omit<Card, "_id">, string>
  | "question.text" | "question.image" | "question.sound" | "question.hint"
  | "answer.texts" | "answer.choices" | "answer.image" | "answer.hint"

export default class CardService {
  static async getCardsByParent(args: { parentId: string }) {
    const cards = await CardModel.find({ parentId: args.parentId, status: STATUS_PUBLIC });
    return cards.map((e) => new Card(e));
  }

  static async updateCardById(args: { _id: string; updates: { [key in CardFields]?: any } }) {
    const updates = _.pick<{ [key in CardFields]?: any }, CardFields>(args.updates, [
      "answer.texts", "answer.image", "answer.choices", "answer.hint",
      "question.text", "question.image", "question.sound", "question.hint",
      "hasChild", "parentId", "type"
    ])
    const newCard = await CardModel.findByIdAndUpdate(args._id, { $set: updates }, { new: true });
    return newCard ? new Card(newCard) : null;
  }

  static async syncCardsSound(args: { cardIds: string[] }) {
    const cards = await CardModel.find({ _id: { $in: args.cardIds } });
    const data = await Promise.all(cards.map(async (_card) => {
      const vocab = await VocabService.getVocabByText({ text: _card.question.text });
      const card = await CardModel.findByIdAndUpdate(_card._id, { $set: { "question.sound": vocab?.soundUrl ?? "" } }, { new: true });
      if (!card) return null;
      const returnCard: Card & { syncUrl?: string } = new Card(card);
      returnCard.syncUrl = vocab?.soundUrl;
      return returnCard;
    }));
    return data.filter((e) => !!e);
  }
}