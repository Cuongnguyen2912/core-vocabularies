import { NextFunction, Request, Response } from "express";

export const errorHandler = (err: Error, req: Request, res: Response, next: NextFunction) => {
  if (err) {
    console.error(`[ERROR] - ${new Date().toISOString()} - from ${req.method} ${req.originalUrl} - `, err.message, err.stack);
    return res.status(500).json({ message: 'Internal Server Error' });
  }
  return next();
}

export const notFoundHandler = (req: Request, res: Response, next: NextFunction) => {
  res.status(404).json({
    success: false,
    message: `Endpoint ${req.method} ${req.url} Not Found`
  });
  return next();
};